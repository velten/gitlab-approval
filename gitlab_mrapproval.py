#!/usr/bin/python
import gitlab
import os
import sys

approvers = None
if os.getenv("APPROVERS"):
    approvers = os.getenv("APPROVERS").split(",")

server_url = os.getenv("CI_PROJECT_URL").replace(os.getenv("CI_PROJECT_PATH"), "")

gl = gitlab.Gitlab(server_url, private_token=os.getenv("API_TOKEN"))

project = gl.projects.get(os.getenv("CI_MERGE_REQUEST_PROJECT_ID"))
mr = project.mergerequests.get(os.getenv("CI_MERGE_REQUEST_IID"))

min_access_level = 40
if os.getenv("ALLOW_DEV_APPROVAL") == "1":
    print("Developers allowed to approve")
    min_access_level = 30

if not approvers:
    approvers = set(map(lambda m: m["username"],
                        filter(lambda m: m["access_level"] >= min_access_level,
                               project.members.all(all=True))))

if not os.getenv("ALLOW_SELF_APPROVAL") == "1":
    approvers.remove(mr.author["username"])
else:
    print("Self approval allowed")

print("Final approvers list: {}".format(approvers))

review_status = 0
for emoji in mr.awardemojis.list():
    if emoji.user["username"] in approvers:
        if emoji.name == "thumbsup":
            review_status += 1
        elif emoji.name == "thumbsdown":
            review_status -= 1

print("Review status: {}".format(review_status))

try:
    approval_threshold = int(os.getenv("APPROVAL_THRESHOLD"))
except Exception:
    approval_threshold = 1
print("Approval threshold: {}".format(approval_threshold))

# the error code reports the number of missing approvals
if review_status < approval_threshold:
    exit(approval_threshold - review_status)

exit(0)
