FROM python:3-alpine

RUN pip install python-gitlab

ADD gitlab_mrapproval.py /usr/bin
RUN chmod +x /usr/bin/gitlab_mrapproval.py
RUN ln -s /usr/local/bin/python /usr/bin/python

CMD /usr/bin/gitlab_mrapproval.py
